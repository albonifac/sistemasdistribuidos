package socket;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 *
 * @author Aldemon
 */
public class Server {
    
    /**
     * Runs the server.
     * @param args
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        try (ServerSocket listener = new ServerSocket(9000)) {
            while (true) {
                try (Socket socket = listener.accept()) {
                    PrintWriter out =
                        new PrintWriter(socket.getOutputStream(), true);
                    out.println(new Date().toString());
                }
            }
        }
    }
    
}
