package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author Aldemon
 */
public class Client {

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1", 9000);
        BufferedReader input =
            new BufferedReader(new InputStreamReader(socket.getInputStream()));
        String buffer = input.readLine();
        System.out.println("Resposta do servidor: '" + buffer + "'");
    }

}
