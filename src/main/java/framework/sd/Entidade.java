/*
 * FRAMEWORK:
 * Essa classe implementa o conceito de ENTIDADE de um protocolo.
 */

package framework.sd;

public class Entidade { 
    
    public Estado estadoCorrente;
    public Msg com;
    public Thread thread;
    public SocketThread xthread; 
    public SThread sthread;
    
    // thread atual para leitura de texto no STDIN
    Thread leTextoAtual = null;
    LeTextoStdinThread leTextoRunnable;
    
    @SuppressWarnings("CallToThreadStartDuringObjectConstruction")
    public Entidade(int _lport){
        com =new Msg(_lport);
                // Inicia thread de leitura do socket
        xthread = new SocketThread(com, this);
        thread = new Thread(xthread);
        thread.start();
    }
    
    // Sincronização necessária para não haver
    // inconsistência na manipulação dos estados
    // do protocolo
    synchronized void transicao(Evento _e){
        estadoCorrente.transicao(_e);
    }

    public void mudaEstado(Estado e){
        System.out.println(e.getClass());
        estadoCorrente = e;
        e.executa();
    }
   
    public void disparaEvento(Evento _e){
        // Inicia thread de tratamento de eventos
        sthread = new SThread(this,_e);
        thread = new Thread(sthread);
        thread.start(); 
    }
   
    public void disparaTimer(Timeout _t){
        // timer
        thread = new Thread(_t);
        thread.start();   
    }
    
    public void criaProcessoLeTexto(Entidade ent, String titulo, int codEvento) {
        
        // cancela o proceso de leitura anterior.
        if (this.leTextoAtual != null) {
            try {
                this.leTextoRunnable.ignorarLeitura();
            }
            finally {}
            try {
                this.leTextoAtual.interrupt();
            }
            finally {}
        }
        
        LeTextoStdinThread leTextoStdinThread = new LeTextoStdinThread(ent, titulo, codEvento);
        Thread leTextoThread = new Thread(leTextoStdinThread);
        
        // guarda o novo processo de leitura;
        this.leTextoAtual = leTextoThread;
        this.leTextoRunnable = leTextoStdinThread;
        
        // -
        leTextoThread.start();
        
    }

}
