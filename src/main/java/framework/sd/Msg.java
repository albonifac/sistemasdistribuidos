/*
 * FRAMEWORK:
 * Essa classe implementa operações
 * necessárias para comunicação via SOCKET.
 */ 
package framework.sd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Msg {
    //Declaro o socket cliente  
    Socket s1 = null, s2=null;        
    //Declaro a Stream de saida de dados  
    PrintStream ps = null;  
    //Declaro o ServerSocket  
    ServerSocket serv=null;   
    //Declaro o leitor para a entrada de dados  
    BufferedReader entrada=null;
    public int lPort;
        
    public Msg(int _lp){
        lPort=_lp;       
    }
    // Para Envio
    public int conecta(String _host, int _dest){
        int ret = 0;
            try { //Cria o socket com o recurso desejado na porta especificada 
                s1 = new Socket(_host,_dest);
                //System.out.println("Conectado a porta "+_dest);
            } catch (UnknownHostException e2) {
                // TODO Auto-generated catch block
                Logger.getLogger(Msg.class.getName()).log(Level.SEVERE, null, e2);
            } catch (IOException e2) {
                // TODO Auto-generated catch block
                Logger.getLogger(Msg.class.getName()).log(Level.SEVERE, null, e2);
                //System.out.println("Tentando nova conexao");
                ret=1;  
            }
            return ret;
    }
      
    public void termina(){
         //Encerra o socket cliente  
           try {           
                    //if (serv!=null)
                    //    serv.close();
                    s1.close();
            } catch (IOException e1) {
                    // TODO Auto-generated catch block
            }		
    }
    
    public void fecha_leitura(){
        try {
            serv.close();
        } catch (IOException ex) {
            Logger.getLogger(Msg.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public synchronized void envia (String _host, int _port, Evento _evento){
        conecta(_host,_port); 
        envia1(_evento.toString());
        termina(); 
    }
	
    public int envia1( String _msg) {
        int ret=0;
        //Cria a Stream de saida de dados 
        try {
            ps = new PrintStream(s1.getOutputStream());
            //Imprime uma linha para a stream de saída de dados  
            ps.print(_msg);  
        } catch (IOException e2) {
            // TODO Auto-generated catch block
            Logger.getLogger(Msg.class.getName()).log(Level.SEVERE, null, e2);
            ret=1;
        }					
        return ret;							         
    }
    
    // Para recebimento
    public int conecta(int _to){
        int ret = 0;
        try {
            serv = new ServerSocket(lPort);	
            //Aguarda uma conexão na porta especificada e cria retorna o socket que irá comunicar com o cliente
            //System.out.println("Wainting for connection\n");
            serv.setSoTimeout(_to);
            s2 = serv.accept();  	              
            //Cria um BufferedReader para o canal da stream de entrada de dados do socket s  
            //entrada = new BufferedReader(new InputStreamReader(s2.getInputStream()));  
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Logger.getLogger(Msg.class.getName()).log(Level.SEVERE, null, e);
            ret=1;
        }  	 
        return ret;
    }  

    public String recebe() {
        String msg = null;		              
        //Cria o ServerSocket na porta 7000 se estiver disponível  
        try {               	              
            //Aguarda por algum dado e imprime a linha recebida quando recebe 
            entrada = new BufferedReader(new InputStreamReader(s2.getInputStream()));
            msg = entrada.readLine();  
        } catch (IOException e) {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            Logger.getLogger(Msg.class.getName()).log(Level.SEVERE, null, e);
            System.out.println("Erro no recebimento");
        }  	 
        return msg;
    }
	
}

