/*
 * FRAMEWORK:
 *   Classe para tratamento de leitura de texto
 *   digitado pelo usuário no STDIN.
 */
package framework.sd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LeTextoStdinThread implements Runnable {
    
    private final Entidade ent;
    private final String titulo;
    private final int codEvento;
    private boolean continuaLeitura;
    
    public LeTextoStdinThread(Entidade ent, String titulo, int codEvento) {
        this.ent = ent;
        this.titulo = titulo;
        this.codEvento = codEvento;
    }

    // interrompe a leitura desse texto digitado pelo usuario.
    public void ignorarLeitura() {
        continuaLeitura = false;
    }
    
    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void run() {
        
        continuaLeitura = true;
        
        // apresenta o texto para o usuario
        System.out.print(this.titulo);
        
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            String buffer = "";
            try {
                
                // aguarda ter algo para ler.
                while (! br.ready()) {
                    Thread.sleep(100);
                    if (! continuaLeitura || Thread.interrupted()) {
                        break;
                    }
                }
                
                // se não foi interrompido, então lê o texto.
                if (continuaLeitura && ! Thread.interrupted()) {
                    buffer = br.readLine();
                }
                
            } catch (InterruptedException e) {
                //System.out.println("ConsoleInputReadTask() cancelled");
            }
            
            // define o novo evento com os dados passados.
            if (continuaLeitura && ! Thread.interrupted()) {
                Evento e = new Evento(codEvento, buffer);
                this.ent.disparaEvento(e);
            }
            
        } catch (IOException ex) {
            //Logger.getLogger(LeTextoStdinThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}