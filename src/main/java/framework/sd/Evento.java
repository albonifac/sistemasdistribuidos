/*
 * FRAMEWORK:
 * Classe que implementa o conceito de EVENTO.
 */
package framework.sd;

public class Evento {
    public int code;
    public String msg;
    public Evento(int _c, String _m){
        code=_c;
        msg=_m;
    }
    @Override
    public String toString(){
        return String.valueOf(code)+","+msg;    
    }
}
