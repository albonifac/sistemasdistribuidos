/*
 * FRAMEWORK:
 *   Classe para tratamento do buffer de mensagens
 *   do Socket.
 */
package framework.sd;

public class SThread implements Runnable {
    public Entidade ent;
    Evento e;
    public SThread(Entidade _ent, Evento _e) {
      ent=_ent;
      e=_e;
    }
    @Override
    public void run() {   
        ent.transicao(e);
    }
}