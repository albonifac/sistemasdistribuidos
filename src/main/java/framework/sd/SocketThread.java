/*
 * FRAMEWORK:
 * Essa classe captura o comportamento comum aos
 * processos de receber as PDU's dos protocolos.
 */
package framework.sd;
public class SocketThread implements Runnable {
    
    Msg ms;
    public Entidade ent;
    public String tmp;
    public Boolean continua = false;
    
    public SocketThread(Msg _m, Entidade _u){
        ms = _m;
        ent = _u;
        continua = true;
    }
    
    @Override
    public void run(){
        String [] split;
        Evento e;
            while (continua) {
                tmp=null;
                ms.conecta(0);
                tmp = ms.recebe(); 
                ms.fecha_leitura();
                // System.out.println("Mensagem recebida: "+tmp);
                if (tmp!=null){
                    e = desempacota();
                    // Inicia thread de tratamento de eventos
                    ent.disparaEvento(e);
                }
                else
                    break;
            }
    }
    
    public void para(){
        continua=false;
    }
    
    public Evento desempacota(){
        // Desempacota mensagem
        String [] split = tmp.split(",");
        int code = Integer.valueOf(split[0]);
        String msg = split.length > 1 ? split[1] : "";
       // Cria Evento
        Evento e = new Evento(code, msg);
       // Coloca no buffer da entidade
       return e;
    }

}
