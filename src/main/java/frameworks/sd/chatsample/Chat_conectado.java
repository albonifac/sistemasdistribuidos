/*
 * Template:
 * Estado IDLE da entidade MEIO
 */
package frameworks.sd.chatsample;

import framework.sd.Entidade;
import framework.sd.Estado;
import framework.sd.Evento;

public class Chat_conectado extends Estado{
    private final Chat chat;
    public Chat_conectado (Entidade _e){
        super(_e);
        chat = (Chat)ent;
    }
    @Override
    public void transicao(Evento _ev){
        Evento e;
        switch(_ev.code){
            case Chat.msg_us:
                e = new Evento(Chat.msg_ep,_ev.msg);
                chat.com.envia("localhost", Integer.parseInt(chat.pr),e);
                // muda estado
                chat.mudaEstado(chat.conectado);
                break;
            case Chat.msg_ep: 
                System.out.println("> "+_ev.msg);
                break;
            case Chat.des_ep:
                // muda estado
                chat.mudaEstado(chat.idle);
                break;
            case Chat.des_us:
                e = new Evento(Chat.des_ep,"");
                chat.com.envia("localhost", Integer.parseInt(chat.pr),e);
                // muda estado
                chat.mudaEstado(chat.idle);
                break;
            default:// evento inesperado
                System.out.println("Chat descartou evento : "+_ev.code + " em Conectado");
        }
    }
       @Override
    public void executa(){
        chat.criaProcessoLeTexto(ent, "Informe a mensagem: ", Chat.msg_us);
    }

}
