/*
 * Template:
 * Estado IDLE da entidade MEIO
 */
package frameworks.sd.chatsample;

import framework.sd.Entidade;
import framework.sd.Estado;
import framework.sd.Evento;

public class Chat_idle extends Estado{
    private final Chat chat;
    public Chat_idle (Entidade _e){
        super(_e);
        chat = (Chat)ent;
    }
    @Override
    public void transicao(Evento _ev){
        Evento e;
        switch(_ev.code){
            // Envia pedido de conexao ao outro usuario
            case Chat.conecta_us:
                // porta do outro usuario
                chat.pr = _ev.msg;
                // cria o evento para conexao
                e = new Evento(Chat.conecta_ep, String.valueOf(chat.com.lPort));
                chat.com.envia("localhost", Integer.parseInt(chat.pr),e); 
                // Timer
                chat.disparaTimer(chat.t1);
                // muda estado
                chat.mudaEstado(chat.conectando);
                break;
            // Retorna confirmacao do pedido de conexao pelo outro usuario
            case Chat.conecta_ep:
                // porta do outro usuario
                chat.pr = _ev.msg;
                // cria evento para conexao
                e = new Evento(Chat.ok, String.valueOf(chat.com.lPort));
                ent.com.envia("localhost", Integer.parseInt(chat.pr),e); 
                // muda estado
                ent.mudaEstado(chat.conectado);
                break;
            default:// evento inesperado
                System.out.println("Chat descartou evento : "+_ev.code + " em IDLE");
        }
    }
    @Override
    public void executa(){
        chat.criaProcessoLeTexto(ent, "Informe a porta do parceiro: ", Chat.conecta_us);
    }

}
