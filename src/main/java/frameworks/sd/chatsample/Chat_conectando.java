/*
 * Template:
 * Estado IDLE da entidade MEIO
 */
package frameworks.sd.chatsample;

import framework.sd.Entidade;
import framework.sd.Estado;
import framework.sd.Evento;

public class Chat_conectando extends Estado{
    private final Chat chat;
    public Chat_conectando (Entidade _e){
        super(_e);
        chat = (Chat)ent;
    }
    @Override
    public void transicao(Evento _ev){
        switch(_ev.code){
            case Chat.timeout:
                chat.mudaEstado(chat.idle);
                break;
            case Chat.ok:  
                // para timer
                chat.t1.paraTimer();
                System.out.println("Conexao realizada");
                chat.mudaEstado(chat.conectado);
                break;
            default:// evento inesperado
                System.out.println("Chat descartou evento : "+_ev.code + " em Conectando");
        }
    }
    @Override
    public void executa(){
    
    }  
}
