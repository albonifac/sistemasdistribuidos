/*
 * Template:
 * Essa classe implementa as características específicas
 * da entidade de protocolo MEIO
 */
package frameworks.sd.chatsample;

import framework.sd.Entidade;
import framework.sd.Estado;
import framework.sd.Timeout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Chat extends Entidade {
    // evento
    public static final int conecta_us = 1;
    public static final int des_us = 2;
    public static final int timeout = 3;
    public static final int des_ep = 4;
    public static final int ok = 5;
    public static final int msg_us = 6;
    public static final int msg_ep = 7;
    public static final int conecta_ep = 8;
    Timeout t1;
    public String ms;
    // apontadores para as entidades relacionadas
    public String pr;
    // apontadores para os estados da entidade
    Estado idle;
    Estado conectando;
    Estado conectado;
    
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public static void main(String args[]) {
        // porta local, porta do sender, porta do receiver
        String _lp = lePorta();
        new Chat(Integer.parseInt(_lp));
    }
    
    public Chat(int _pl){
        super(_pl);
        t1 = new Timeout(this, 10000);
        idle = new Chat_idle(this);
        conectando = new Chat_conectando(this);
        conectado = new Chat_conectado(this);
        mudaEstado(idle);
        System.out.println("Entidade Chat inicializada na porta "+_pl);
    }
    
    private static String lePorta(){
        String aux;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in)); 
        System.out.print("Informe a porta de escuta: ");
        try {
            aux = in.readLine();
            return aux;
        } catch (IOException ex) {
            Logger.getLogger(Chat_idle.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
}
